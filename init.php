<?php

  function provision($username, $password) {
    // declare the provisioning JSON
    $json = '
    {
      "timeout": 1,
      "webAppId": "my-web-app-id123",        
      "voice": {
      "username": "%s",
      "domain": "example.com",
      "inboundCallingEnabled": false,
      "allowedOutboundDestination": "all",
      "auth": {
        "username": "%s",
        "password": "%s",
        "realm": "example.com"
        }
      },
      "urlSchemeDetails": {
        "host": "example.com",
        "port": "8443",
        "secure": true
      }
    }';

    // inject the provisioning JSON with the params passed to the function
    $json = sprintf($json, $username, $username, $password);

    // configure the curl options
    $ch = curl_init('http://example.com:8080/gateway/sessions/session');    
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
    curl_setopt($ch, CURLOPT_HTTPHEADER, [                
      'Content-Type: application/json',
      'Content-Length: ' . strlen($json)    
    ]);


    // execute HTTP POST & close the connection    
    $response = curl_exec($ch);  
    curl_close($ch);

    // return the response from the Gateway
    return $response;
  }

?>